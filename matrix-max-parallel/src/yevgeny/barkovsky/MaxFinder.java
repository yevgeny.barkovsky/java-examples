package yevgeny.barkovsky;

/**
 * Copyright DonRiver Inc. All Rights Reserved.
 * <p>
 * Author: Yevgeny Barkovsky
 * Created: 22.05.2019
 */
public final class MaxFinder {
    public static int find(int[] arr) {
        int max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }

        return max;
    }
}
