package yevgeny.barkovsky;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;

public class Main {

    public static void main(String[] args) throws IOException, InterruptedException, ExecutionException {
        if (args.length == 0) {
            throw new IllegalArgumentException("You must specify matrix size");
        }
	    int matrixSize = Integer.valueOf(args[0]);

        // store matrix to temp file
        final Path path = Files.createTempFile("matrix", ".tmp");
        FileWriter fileWriter = new FileWriter(path.toFile());

        // generate matrix and write it to the file
        try(BufferedWriter writer = new BufferedWriter(fileWriter)) {
            Random random = new Random();
            for (int i = 0; i < matrixSize; i++) {
                for (int j = 0; j < matrixSize; j++) {
                    writer.append(String.valueOf(random.nextInt(500000))).append(" ");
                }
                writer.append(System.lineSeparator());
            }
        }

        // read file and convert it to matrix of ints. Don't check sizes.
        final List<String> lines = Files.readAllLines(path);
        int[][] matrix = new int[matrixSize][matrixSize];
        for(int i = 0; i < lines.size(); i ++) {
            String[] digits = lines.get(i).split(" ");
            for (int j = 0; j < digits.length; j++) {
                matrix[i][j] = Integer.valueOf(digits[j]);
            }
        }

        // will store row results here
        int[] rowResults = new int[matrixSize];

        // submit max calculation of each line to independent thread
        final ExecutorService threadPool = Executors.newFixedThreadPool(matrixSize);
        CompletionService<Integer> completionService = new ExecutorCompletionService<>(threadPool);
        for(int i = 0; i < matrix.length; i ++) {
            int finalI = i;
            completionService.submit(() -> MaxFinder.find(matrix[finalI]));
        }

        // wait for completion of the each thread
        for(int i = 0; i < matrixSize; i ++) {
            final Future<Integer> result = completionService.take();
            // above call blocks till atleast one task is completed and results available for it
            // but we dont have to worry which one
            rowResults[i] = result.get();
        }

        // now calculate final result
        System.out.println("Maximum of the our matrix is " + MaxFinder.find(rowResults));

        // shutdown theread pool
        threadPool.shutdownNow();
    }
}
